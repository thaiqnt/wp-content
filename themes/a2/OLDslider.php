<!-- Template Name: Slider -->
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
          <ol class="carousel-indicators">
            <?php 
            $posts = get_posts(['category_name' => 'sliders']); 
            $i = 0;
            $active='active';
            ?>
            <?php foreach($posts as $post):?>
            <li data-target="#carouselExampleIndicators" data-slide-to="<?php echo $i++ ?>" <?php echo $active; ?> ></li>
            <?php 
              $active='';
              endforeach;
            ?>
          </ol>
          <div class="carousel-inner">
            <?php 
            $posts = get_posts(['category_name' => 'sliders']);
            $i = 1;
            $active='active';
            ?>
            <?php foreach($posts as $post):?>
            <div class="carousel-item <?php echo $active; ?>">
              <a href="<?=get_the_permalink($post->ID)?>">
              <img class="d-block w-100" src="<?=get_template_directory_uri()?>/img/slider<?php echo $i ?>.jpg" alt="slider<?php echo $i++ ?>.jpg">
              </a>
              <div class="carousel-caption d-none d-md-block overrainbow">
              </div>              
            </div>
            <?php 
              $active='';
              endforeach;
            ?>
          </div>
          <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
