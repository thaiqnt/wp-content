
  <div id="footer" class="onecol">
    <div class="wrapper wrapper-footer">
      <header class="site-footer">

        <?php 
        $args = array(
          'theme_location' => 'footer'
          ); 
        ?>

        <nav class="site-nav">
          <?php wp_nav_menu($args) ?>
        </nav>
      </header>

    </div>
    <div class="wrapper wrapper-bottom">
      <header class="site-footer">
        <?php 
        $args = array(
          'theme_location' => 'bottom'
          ); 
        ?>

        <nav class="site-nav">
          <?php wp_nav_menu($args) ?>
        </nav>

      </header>
    </div>
    <p id="copyright">Content Copyright by 2010 - 2014 by Westland Partners</p>
  </div>


  </div>
  <!-- end of content-wrap -->
</div>
<!-- end of content -->

<?php wp_footer(); ?>

</body>
</html>
