<?php get_header(); ?>

<div class="onecol">
    <header>
      <?php get_template_part('slider'); ?>
    </header>
</div>

<div class="onecol">
    <?php $posts = get_posts(['category_name' => 'features']); ?>
    <?php foreach($posts as $post):?>
      <div class="article-content3">
        <div>
          <a href="<?=get_the_permalink($post->ID)?>"><?=get_the_post_thumbnail($post->ID, 'medium')?></a>
        </div>


        <h3><a href="<?=get_the_permalink($post->ID)?>"><?=$post->post_title?></a></h3>

        <p><?=strip_tags(substr($post->post_content, 0, 155))?></p>
        <a href="<?=get_the_permalink($post->ID)?>" class="readmore"><img src="<?=get_template_directory_uri()?>/img/readmore.jpg" alt="read more image" /></a>
      </div>
    <?php endforeach?>
</div>


<?php get_footer(); ?>