<?php 
// Load thumbnail
if( function_exists( 'add_theme_support' ) ) {

	add_theme_support('post-thumbnails');

	add_image_size( 'list-thumb', 75, 75 );

}

// Load menu
if( function_exists( 'register_nav_menus' ) ) {
	register_nav_menus(array(
      'top' => __('Top Menu'),
      'primary' => __('Primary Menu'),
      'footer' => __('Footer Menu'),
      'bottom' => __('Bottom Menu')
      ));
}

// Load bootstrap
if(!function_exists('wpa_load_styles')){

	function wpa_load_styles()
	{

		wp_enqueue_style( 'bootstrap', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css', [], '4.1.3', 'all');

		wp_enqueue_style( 'wpa', get_stylesheet_uri(), ['bootstrap'], false, 'all');

		wp_enqueue_script('bootsrapjs', 'https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js', ['jquery'], '4.1.3', true);

	}

	add_action('wp_enqueue_scripts', 'wpa_load_styles');

}


function wpa_list_child_pages() { 
 
global $post; 
$string = '';
  
if ( is_page() && $post->post_parent )
	$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent );
else
	$childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID );
 
if ( $childpages ) {
	$string = '<ul>' . $childpages . '</ul>';
}
  
return $string;
 
}
 
// Shortcode for displaying all sub pages
add_shortcode('wpa_childpages', 'wpa_list_child_pages');




