<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="utf-8" />
	<title><?php bloginfo('name')?></title>

	<link rel="stylesheet" type="text/css" 
			href="<?=get_stylesheet_uri()?>">

<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
  
<div id="content">
  <div class="wrapper-top">
    <header class="site-top">
      <ul class="topitem">
        <li>
          <a href="#"><img style="width: 30px; height: 30px" src="<?=get_template_directory_uri()?>/img/facebook40x40.png" alt="facebook" /></a>
        </li>
        <li>
          <a href="#"><img style="width: 30px; height: 30px" src="<?=get_template_directory_uri()?>/img/twitter40x40.png" alt="twitter" /></a>
        </li>
        <li>
          <?php 
            $args = array(
              'theme_location' => 'top'
            ); 
          ?>
          <nav class="site-nav">
            <?php wp_nav_menu($args) ?>
          </nav>
        </li>
      </ul>

    </header>
  </div>

  <div id="content-wrap">
  <img src="<?=get_template_directory_uri()?>/img/header.jpg" alt="home page image" />

  <header class="site-header">


    <?php 
    $args = array(
      'theme_location' => 'primary'
      ); 
    ?>

    <nav class="site-nav">
      <?php wp_nav_menu($args) ?>
    </nav>
  </header>
