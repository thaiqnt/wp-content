<?php get_header(); ?>


<div id="leftcol">
  <?php get_sidebar(); ?>
</div>

<div id="rightcol">
  <h1 class="pageh1"><?php echo get_the_title($post->post_parent); ?></h1>
<br/>
          
  <?php the_title('<h2 class="pageh2">','</h2>'); ?>
  
  <?php if(get_cat_ID($post->post_title)): ?>
      <ul>
      <?php
      $args = array( 'category' => get_cat_ID($post->post_title) );

      $myposts = get_posts( $args );
      foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
          <li style="list-style: none">
            <div class="postbox">
              <div class="postthumb">
                <a href="<?php the_permalink(); ?>"><?=the_post_thumbnail('thumbnail')?></a>
              </div>
              <div class="postcontent">
                <h5><a href="<?php the_permalink(); ?>"><?php echo get_the_title(); ?></a></h5>

                <p><a href="<?php the_permalink(); ?>"><?php echo strip_tags(get_the_excerpt()) ?></a></p>
                <a href="<?php the_permalink(); ?>" class="postlearnmore"><strong>Learn more &gt;&gt;&gt;</strong></a>
              </div>
            </div>

          </li>
      <?php endforeach; 
      wp_reset_postdata();?>
      </ul>    
   <?php else: ?>
		<div class="postitem">

			<?php while(have_posts()) : ?>
	
				<?php the_post(); ?>

				<p><small>Posted on <?php the_date(); ?> at <?php the_time() ?> by <?php the_author() ?></small></p>

				<?php the_content('<blockquote>', '</blockquote>'); ?>
					

			<?php endwhile; ?>

		</div><!-- /postitem -->

   <?php endif; ?>

</div>




<?php get_footer(); ?>