<!-- <?=__FILE__?> -->
<?php get_header(); ?>

		<div class="postitem">

			<?php while(have_posts()) : ?>
	
				<?php the_post(); ?>

				<a href="<?php the_permalink() ?>"><?php the_title('<h2 class="pageh2">','</h2>'); ?></a>

				<p><small>Posted on <?php the_date(); ?> at <?php the_time() ?> by <?php the_author() ?></small></p>

				<?php the_content('<blockquote>', '</blockquote>'); ?>
					

			<?php endwhile; ?>

		</div><!-- /main -->

<?php get_footer(); ?>