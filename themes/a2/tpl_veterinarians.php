<?php
// Template Name: Veterinarians Template

get_header(); ?>


<div id="leftcol">
  <?php get_sidebar(); ?>
</div>

<div id="rightcol">
  <h1 class="pageh1"><?php echo get_the_title($post->post_parent); ?></h1>
<br/>
          
  <?php the_title('<h2 class="pageh2">','</h2>'); ?>
  <blockquote>
    <?=strip_tags($post->post_content)?>
  </blockquote>

<ul>
<?php
$comments = get_comments( array( 'post_id' => get_the_ID() ) );
foreach ( $comments as $comment ) :
?>
          <li style="list-style: none">
            <div class="vetbox">
              <?php
              if(($avatar = get_avatar($comment->comment_author_email, 150)) !== FALSE):
              ?>
              <div class="vetthumb">
                <?=$avatar?>
              </div>
              <?php
              endif;
              ?>
              <div class="vetcontent">
                
                <blockquote><?=strip_tags($comment->comment_content)?>
                <br/>
                <?=$comment->comment_author?>
                </blockquote>
              

              </div>
            </div>

          </li>
<?php
endforeach;
?>
</ul>

<!-- This code below for experiment purpose only.
  <?php if(get_cat_ID($post->post_title)): ?>
      <ul>
      <?php
      $args = array( 'category' => get_cat_ID($post->post_title) );

      $myposts = get_posts( $args );
      foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
          <li style="list-style: none">
            <div class="vetbox">
              <?php
              if (has_post_thumbnail()):
              ?>
              <div class="vetthumb">
                <a href="<?php the_permalink(); ?>"><?=the_post_thumbnail('thumbnail')?></a>
              </div>
              <?php
              endif;
              ?>
              <div class="vetcontent">
                <p><a href="<?php the_permalink(); ?>">
                  <blockquote><?=strip_tags(the_content())?>
                <br/>
                <?php the_title(); ?>
                </blockquote>
                </a></p>

              </div>
            </div>

          </li>
      <?php endforeach; 
      wp_reset_postdata();?>
      </ul>    
   <?php endif; ?>
-->

</div>




<?php get_footer(); ?>